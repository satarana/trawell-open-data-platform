const jwt = require('jsonwebtoken');
const config = require('../config/config.json')

function genToken (data) {
    return jwt.sign({ data: data.email, role: 'user' }, config.secret_Key, { expiresIn: '7d' });
}

function verify (req, res, next){
    const gettoken = req.body.headers.Authorization
    jwt.verify(gettoken, config.secret_Key, (err, decoded) => {
        if (decoded){
            next() 
            return 
        } 
            res.send({message:'unauthorized'})
    });  
}

function getUser (token) {
    return new Promise((res, rej) => {
        const gettoken = token.headers.Authorization
        jwt.verify(gettoken, config.secret_Key, (err, decoded) => {
            console.log(decoded.data)
            res(decoded)
        });
        
    })
}

module.exports = { genToken, verify, getUser }