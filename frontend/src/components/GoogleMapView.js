import React from 'react'
import { Card, Image, Rating, Dropdown, Button } from 'semantic-ui-react'

const { compose, withProps, withHandlers, lifecycle, withState } = require("recompose");
const { withScriptjs, withGoogleMap, GoogleMap, Marker } = require("react-google-maps");
const { MarkerClusterer } = require("react-google-maps/lib/components/addons/MarkerClusterer");
const { SearchBox } = require("react-google-maps/lib/components/places/SearchBox");
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");
const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");

const GoogleMapView = compose(
  withState('infoData', 'setInfoData', {}),
  withState('infoData', 'setInfoData', {}),
  withState('isInfo', 'setIsInfo', false),
  withState('isLocationLoadding', 'setIsLocationLoadding', false),
  withState('posData', 'setPosData', {
      Center: {lat: 13.667837, lng: 100.558402}, 
      Location: {lat: 13.667837, lng: 100.558402},
      zoom: 10,
      isLocation: false
  }),
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAPMmzFbK9UyTCmH0Tn7lRvJc_KTV4ASE0&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ width: `100%`, height: `500px`, marginTop: `55px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withHandlers({
    onMarkerClustererClick: () => (markerClusterer) => {
      const clickedMarkers = markerClusterer.getMarkers()
      console.log(`Current clicked markers length: ${clickedMarkers.length}`)
      console.log(clickedMarkers)
    },
    openInfoClick: ({isInfo, setIsInfo, infoData, setInfoData}) => (params) => {
      
      setInfoData(params)
      if(isInfo === true) {
        setIsInfo(false)
      }
      if(isInfo === false) {
          setIsInfo(true)
      }
    },
    closeInfo: ({setIsInfo}) => (param) => {
      setIsInfo(false)
    },
    getLocation: ({isLocationLoadding, setIsLocationLoadding, posData, setPosData}) => () => {
        setIsLocationLoadding(true)
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                setIsLocationLoadding(false)
                console.log('GoogleMapView', position)
                setPosData({
                    Center: {lat: position.coords.latitude, lng: position.coords.longitude}, 
                    Location: {lat: position.coords.latitude, lng: position.coords.longitude},
                    zoom: 14,
                    isLocation: true
                })
                console.log(posData)
                return 
            })
        }
        
    }
  }),
  lifecycle({
    componentDidMount() {
        // this.props.getLocation()
    }
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    zoom={props.posData.zoom}
    center={props.posData.Center}
    defaultOptions={{disableDefaultUI: true}}
  >
    <MarkerClusterer
      onClick={props.onMarkerClustererClick}
      averageCenter
      enableRetinaIcons
      gridSize={60}
    >
      {props.markers.map((marker, index) => (
        <Marker
          key={index}
          position={{ lat: (parseFloat(marker.lat)), lng: (parseFloat(marker.lng)+0.0022582) }}
          labelAnchor={new window.google.maps.Point(0, 0)}
          labelStyle={{fontSize: "12px", color: "#769bc9"}}
          onClick={() => props.openInfoClick(marker)}
        />
      ))}
    </MarkerClusterer>
    {props.isInfo && <InfoBox
                      defaultPosition={new window.google.maps.LatLng(parseFloat(props.infoData.lat), parseFloat(props.infoData.lng)+0.0022582)}
                      onCloseClick={() => props.closeInfo()}
                      options={{ closeBoxURL: ``, enableEventPropagation: true }}
                      >
                      <Card>
                      <Image src='https://media.timeout.com/images/104712785/630/472/image.jpg' />
                        <Card.Content header={props.infoData.placename} />
                        <Card.Content>
                        <p><strong>รายระเอียด :</strong>  {props.infoData.about_the_place} </p>
                        <p><strong>เปิด-ปิด :</strong>  {props.infoData.operatingdate} {props.infoData.operatingtime}</p>
                        <p><strong>by :</strong>  {props.infoData.create_by}</p>
                        </Card.Content>
                        <Card.Content extra>
                          <Rating icon='star' defaultRating={3} maxRating={4} />
                        </Card.Content>
                      </Card>
                     </InfoBox>
    }
                    
    { props.posData.isLocation && <MarkerWithLabel
                              position={props.posData.Center}
                              labelAnchor={new window.google.maps.Point(0, 0)}
                              icon={{
                                  url: 'http://icon-park.com/imagefiles/location_map_pin_navy_blue5.png',
                                  scaledSize: new window.google.maps.Size(32, 32),
                              }} 
                              >
                              <span></span>
                          </MarkerWithLabel>
     }

    <Dropdown placeholder='Search box' search selection
                style={{
                    position: 'absolute',
                    zIndex: 1,
                    top: '61px',
                    left: '5px',
                    width: '300px',
                    height: '36px',
                    borderRadius: 0}}
                />

    <Button icon='crosshairs' onClick={() => props.getLocation()} loading={props.isLocationLoadding}
                    style={{
                        background: 'white',
                        boxShadow: '0 1px 0 1px rgba(255, 255, 255, 0.3) inset, 0 0 0 1px #ffffff inset',
                        position: 'absolute',
                        zIndex: 1,
                        top: '62px',
                        left: '306px',
                        borderRadius: 0}}/>
   
  </GoogleMap>
);

export default GoogleMapView