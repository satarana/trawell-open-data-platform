import React from 'react'
import axios from 'axios'
import { GoogleMapView } from '../components'

class MapView extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      markers: []
    }
  }

  componentDidMount() {
      this.apiDataMaker()
  }

  apiDataMaker = () => {
    axios.get('http://139.5.146.106:3000/google/api')
      .then((res) => {
          this.setState({ markers: res.data })
      })
      .catch((error) => {
          console.log(error)
      })
  }

  render() {
    let { markers } = this.state
    return (
        <div>
            <GoogleMapView markers={markers}/>
        </div>
    )
  }
}
  
export default MapView
