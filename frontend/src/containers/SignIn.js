import React, { Component } from 'react';
import { Button, Checkbox, Form, Container, Grid, Segment, Icon } from 'semantic-ui-react'
import axios from 'axios'

class SignIn extends Component {

  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: ''
    }

  }

  changeStateInput = (e) => {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  submitFrom = () => {
    console.log(this.state)

    axios.post('http://139.5.146.106:3000/login', {
      email: this.state.email,
      password: this.state.password,
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });

  }

  render() {
    return (
        <Container style={{marginTop: '90px'}}>
            <Grid>
                <Grid.Column>
                  <Segment.Group>
                    <Segment>Sign In</Segment>
                    <Segment>
                      <Form>
                        <Form.Field>
                          <Form.Input 
                              fluid label='E-mail' 
                              placeholder='E-mail'
                              name="email"
                              onChange={this.changeStateInput}
                            />
                        </Form.Field>
                        <Form.Field>
                          <Form.Input 
                              fluid label='Password' 
                              placeholder='Password' 
                              type='password'
                              name="password"
                              onChange={this.changeStateInput}
                            />
                        </Form.Field>
                        <Button color='facebook'>
                          <Icon name='facebook' /> Sign In with Facebook
                        </Button>
                        <Button style={{float: 'right'}} onClick={() => this.submitFrom()}>Sign In</Button>
                      </Form>
                    </Segment>
                  </Segment.Group>
                </Grid.Column>
            </Grid>
          
        </Container>
    )
  }
};

export default SignIn