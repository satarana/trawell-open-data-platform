import React, { Component } from 'react'
import { Button, Checkbox, Form, Container, Grid, Segment, Icon } from 'semantic-ui-react'
import axios from 'axios'

class SignUp extends Component {

  constructor(props) {
    super(props)

    this.state = {
      first_name: '',
      last_name: '',
      gender: '',
      email: '',
      password: '',
      confirmPassword: '',
    }

  }

  changeStateInput = (e) => {
      const { name, value } = e.target
      this.setState({ [name]: value })
  }

  changeStateDropbox = (e, { value }) => {
    this.setState({gender: value })
  }

  submitFrom = () => {
    console.log(this.state)

    axios.post('http://localhost:3000/register', {
      firstName: this.state.first_name,
      lastName: this.state.last_name,
      gender: this.state.gender,
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword
    })
    .then(function (response) {
      console.log(response);
    })
    .catch(function (error) {
      console.log(error);
    });

  }

  render() {

    const gender_op = [
                       { key: 'male', value: 'male', text: 'Male' },
                       { key: 'female', value: 'female', text: 'Female' }
                      ]

    return (
        <Container style={{marginTop: '90px'}}>
            <Grid>
                <Grid.Column>
                  <Segment.Group>
                    <Segment>Sign Up</Segment>
                    <Segment>
                      <Form>
                        <Form.Group widths='equal'>
                          <Form.Input 
                            fluid label='First name' 
                            placeholder='First name' 
                            name="first_name"
                            onChange={this.changeStateInput}
                          />
                          <Form.Input 
                            fluid label='Last name' 
                            placeholder='Last name'
                            name="last_name"
                            onChange={this.changeStateInput}
                          />
                          <Form.Select 
                            fluid label='Gender' 
                            placeholder='Gender' 
                            options={gender_op}
                            name="gender"
                            onChange={this.changeStateDropbox}
                          />
                        </Form.Group>
                        <Form.Field>
                          <Form.Input 
                            fluid label='E-mail' 
                            placeholder='E-mail'
                            name="email"
                            onChange={this.changeStateInput}
                          />
                        </Form.Field>
                        <Form.Field>
                          <Form.Input 
                            fluid label='Password' 
                            placeholder='Password' 
                            type='password'
                            name="password"
                            onChange={this.changeStateInput}
                          />
                        </Form.Field>
                        <Form.Field>
                          <Form.Input 
                            fluid label='Re-Password' 
                            placeholder='Re-Password' 
                            type='password'
                            name="confirmPassword"
                            onChange={this.changeStateInput}
                          />
                        </Form.Field>
                        <Button color='facebook'>
                          <Icon name='facebook' /> Sign Up with Facebook
                        </Button>
                        <Button style={{float: 'right'}} onClick={() => this.submitFrom()}>Sign Up</Button>
                      </Form>
                    </Segment>
                  </Segment.Group>
                </Grid.Column>
            </Grid>
        </Container>
    )
  }
};

export default SignUp