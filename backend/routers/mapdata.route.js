const { verify } = require('../middleware/auth')

module.exports = (app) => {
    const mapdata = require('../controller/Mapdata')
    app.route('/api/v1/mapdata').post(mapdata.listAll)
    app.route('/api/v1/listbycategory').post(mapdata.listByCategory)

    app.route('/api/v1/mapdata/listByUser').post(verify, mapdata.listByUser)
    app.route('/api/v1/mapdata/creacte').post(verify, mapdata.create)
    app.route('/api/v1/mapdata/deleteByid').post(verify, mapdata.deleteByid)
    app.route('/api/v1/mapdata/updateByid').post(verify, mapdata.updateByid)

    const category = require('../controller/Category')
    app.route('/api/v1/listCategory').post(category.list)

}