const Category = require('../models/category')

exports.list = function(req, res, next) {
    Category.find({}, (err, data) => {
        res.json(data)
    })
}