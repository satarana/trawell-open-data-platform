import React, { Component } from 'react'
import {
    Button,
    Container,
    Menu,
    Responsive,
    Segment,
    Visibility,
    Icon,
    Input,
    Dropdown,
    Sidebar,
    Modal,
    Header
} from 'semantic-ui-react'

import { Link } from 'react-router-dom'

const Navbar = () => (
    <div>
        <Menu fixed='top' style={{height: `55px`}} >
            <Container>
                <Link to="/" style={{marginTop: '10px', height: '10px', adding: 0, margin: 0}}>
                    <Menu.Item as='a' header style={{height: '55px'}}> Trawell Open Data Platform </Menu.Item>
                </Link>
                <Menu.Item as='a'>Home</Menu.Item>

                <Dropdown item simple text='Dropdown'>
                    <Dropdown.Menu>
                        <Dropdown.Item>List Item</Dropdown.Item>
                        <Dropdown.Item>List Item</Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Header>Header Item</Dropdown.Header>
                        <Dropdown.Item>
                        <i className='dropdown icon' />
                        <span className='text'>Submenu</span>
                        <Dropdown.Menu>
                            <Dropdown.Item>List Item</Dropdown.Item>
                            <Dropdown.Item>List Item</Dropdown.Item>
                        </Dropdown.Menu>
                        </Dropdown.Item>
                        <Dropdown.Item>List Item</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>

                <Menu.Item position='right'>
                    <Link to="/signin">
                        <Button className="ui teal basic button" style={{ marginLeft: '0.5em' }} >Sign In</Button>
                    </Link>
                    <Link to="/signup">
                        <Button className="ui teal basic button" style={{ marginLeft: '0.5em' }} >Sign Up</Button>
                    </Link>
                </Menu.Item>
            </Container>
        </Menu>
    </div>
)

export default Navbar
