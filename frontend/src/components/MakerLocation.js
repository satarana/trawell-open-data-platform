import React, { Component } from 'react';
const { MarkerWithLabel } = require("react-google-maps/lib/components/addons/MarkerWithLabel");

class MakerLocation extends Component {
    
    constructor(props) {
        super(props)

        this.state = ({
            position: {lat: 13.667837, lng: 100.558402}
        })
    }

    componentDidMount() {
        this.setState({
            position: {lat: 13.903977999999999, lng: 100.5304118}
        })
    }

    render() {
        let { position } = this.state
        return (
            <MarkerWithLabel
                position={position}
                labelAnchor={new window.google.maps.Point(0, 0)}
                icon={{
                    url: 'http://icon-park.com/imagefiles/location_map_pin_navy_blue5.png',
                    scaledSize: new window.google.maps.Size(34, 32),
                }} 
                > 
                <span></span>
            </MarkerWithLabel>
        )
    }
}

export default MakerLocation
