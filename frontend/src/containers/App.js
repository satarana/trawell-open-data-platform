import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'
import { Map, SignUp, SignIn } from './index'
import { Navbar, GoogleMapView } from '../components'
import { Responsive, Sidebar, Menu, Segment, Icon, Container } from 'semantic-ui-react'

class App extends Component {

  state = { visible: false }

  toggleVisibility = () => this.setState({ visible: !this.state.visible })

  render() {
    return (
      <div>
          <Navbar/>
          <Switch>
            <Route exact path="/" component={Map}/>
            <Route path="/signup" component={SignUp}/>
            <Route path="/signin" component={SignIn}/>
          </Switch>
      </div>
    );
  }
}

export default App;
